# 開発環境構築

## 概要

以下のソフトウェアが必要になる。

- Node.js
- Visutal Studio Code

---

## Node.js

### 概要

フロントのビルドやパッケージ管理に使用する。
[公式サイト](https://nodejs.org/ja/)から最新版を取得する。

### インストール

公式サイトからインストーラを取得してインストールする。

---

## Visual Studio Code

### 概要

フロントの開発に用いる。
[公式サイト](https://code.visualstudio.com/)から取得する。

### インストール

公式サイトからインストーラを取得してインストールする。

### 開発環境の構築

`Ctrl+Shift+X` を押下すると拡張機能のタブが開く.

`@recommended:workspace` で検索して表示された拡張機能を全てインストールする。

Node.js の実行は `Ctrl+@` で開くターミナルから行う。
